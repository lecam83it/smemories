import Foundation
import UIKit

enum AppRoute: Equatable {
  case home
  case camera
  case gallery
  case edit(images: [UIImage])
  
  static func == (lhs: AppRoute, rhs: AppRoute) -> Bool {
    return lhs.key == rhs.key
  }
  
  var key: String {
    switch self {
    case .home:
      return "/home"
    case .edit(images: _):
      return "/edit"
    case .camera:
      return "/camera"
    case .gallery:
      return "/gallery"
    }
  }
}

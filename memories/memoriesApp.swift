import SwiftUI
import UIPilot

@main
struct memoriesApp: App {
  @StateObject var pilot = UIPilot(initial: AppRoute.home)
  var body: some Scene {
    WindowGroup {
      UIPilotHost(pilot) { route in
        switch route {
        case .home:
          return AnyView(HomeView()
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true))
        case .camera:
          return AnyView(CameraView()
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true))
        case .gallery:
          return AnyView(GalleryView()
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true))
        case .edit(images: let images):
          return AnyView(CollapseView(images: images)
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true))
        }
      }
    }
  }
}

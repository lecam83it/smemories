import Foundation
import SwiftUI

enum PhotoOptionBottomSheetPosition: CGFloat, CaseIterable {
  case middle = 285, hidden = 0
}


class HomeViewController: ObservableObject {
  @Published var isModalPresent: PhotoOptionBottomSheetPosition = .hidden
  
  private let cameraManager: CameraManager = CameraManager.shared
  
  func requestCamera() {
    cameraManager.checkPermissions()
    cameraManager.configuration()
  }
}

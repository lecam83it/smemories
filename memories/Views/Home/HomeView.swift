import SwiftUI
import UIPilot
import BottomSheet

struct HomeView: View {
  
  @StateObject var controller = HomeViewController()
  @EnvironmentObject var pilot: UIPilot<AppRoute>
  
  init() {
    CameraManager.shared.checkPermissions()
    CameraManager.shared.configuration()
  }
  
  var body: some View {
    ZStack {
      VStack(spacing: 0) {
        
        Rectangle().fill(.clear).frame(height: 56)
        
        Color.clear.frame(height: 16)
        
        VStack(alignment: .leading, spacing: 0) {
          Text("Savings your memories...")
            .font(.itim(size: 24))
            .padding(.horizontal, 8)
          
          ZStack {
            CarouselView(numberOfItems: 4) {
              CollapseTwoViewCarousel()
              CollapseThreeViewCarousel()
              CollapseFourViewCarousel()
              CollapseFiveViewCarousel()
            }
            .frame(width: UIScreen.width, height: (UIScreen.width * 4 / 3) + 16)
          }
        }
        
        Spacer()
        
        CreatePhotoButton(onPressed: {
          controller.isModalPresent = .middle
        })
      }
      .bottomSheet(
        bottomSheetPosition: $controller.isModalPresent,
        options: [.background({ AnyView(Color.white) }), .backgroundBlur(effect: .dark), .absolutePositionValue, .noBottomPosition, .tapToDismiss, .swipeToDismiss])
      {
        Text("Create memories ...")
          .font(.itim(size: 24))
          .padding(.horizontal, 24)
          .padding(.top, 24)
      } mainContent: {
        HStack {
          TakingPhotoWidget {
            controller.isModalPresent = .hidden
            pilot.push(.camera)
          }
          Spacer()
          ImportingPhotoWidget {
            controller.isModalPresent = .hidden
            pilot.push(.gallery)
          }
        }
        .padding(.horizontal, 48)
      }
    }
  }
}

struct CollapseTwoViewCarousel: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 2 - 1,
              height: geometry.size.height
            )
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 2 - 1,
              height: geometry.size.height
            )
        }
      }
    }
  }
}

struct CollapseThreeViewCarousel: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2
            )
          
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
          }
        }
      }
    }
  }
}

struct CollapseFourViewCarousel: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
          
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3  - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
          }
        }
      }
    }
  }
}

struct CollapseFiveViewCarousel: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 2
            )
          
          
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
        }
      }
    }
  }
}

struct TakingPhotoWidget: View {
  let onPressed: () -> Void
  
  var body: some View {
    Button {
      onPressed()
    } label: {
      VStack(spacing: 0) {
        ZStack {
          RoundedRectangle(cornerRadius: 8)
            .fill(Color.lightGray)
            .frame(width: 72, height: 72)
          Image("camera")
            .resizable()
            .scaledToFit()
            .frame(width: 28, height: 28)
        }
        Text("By taking\nphotos")
          .font(.itim(size: 16))
          .foregroundColor(.blackTitle)
          .multilineTextAlignment(.center)
          .padding(.top, 2)
        
      }
    }
  }
}

struct ImportingPhotoWidget: View {
  let onPressed: () -> Void
  
  var body: some View {
    Button {
      onPressed()
    } label: {
      VStack(spacing: 0) {
        ZStack {
          RoundedRectangle(cornerRadius: 8)
            .fill(Color.lightGray)
            .frame(width: 72, height: 72)
          Image("media")
            .resizable()
            .scaledToFit()
            .frame(width: 28, height: 28)
        }
        Text("By importing\nphotos")
          .font(.itim(size: 16))
          .foregroundColor(.blackTitle)
          .multilineTextAlignment(.center)
          .padding(.top, 2)
      }
    }
  }
}

struct CreatePhotoButton: View {
  
  let onPressed: () -> Void
  
  var body: some View {
    Button {
      onPressed()
    } label: {
      ZStack {
        Circle()
          .fill(.black)
          .frame(width: 64, height: 64)
        
        Circle()
          .strokeBorder(.black, lineWidth: 2)
          .frame(width: 72, height: 72)
      }
    }
  }
}


struct HomeView_Previews: PreviewProvider {
  static var previews: some View {
    HomeView()
  }
}

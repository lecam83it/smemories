import Foundation
import UIKit
import Photos

class GalleryViewModel: ObservableObject {
  @Published var images: [UIImage] = []
  @Published var selectedImages: [UIImage] = []
  @Published var errorMessage: String = ""
  
  init() {
    getLocalImagesHandler()
  }
  
  private func getLocalImagesHandler() {
    // check permission
    requestPermission()
  }
  
  private func requestPermission() {
    PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
      switch status {
      case .authorized:
        DispatchQueue.main.async {
          self.errorMessage = ""
          self.getLocalImages()
        }
        
        return
      case .denied, .restricted:
        DispatchQueue.main.async {
          self.errorMessage = "Photo access permission denied"
          self.images = []
        }
      case .notDetermined:
        DispatchQueue.main.async {
          self.errorMessage = "Photo access permission not determined"
          self.images = []
        }
      default:
        fatalError()
      }
    }
  }
  
  private func getLocalImages() {
    let manager: PHImageManager = PHImageManager.default()
    let options: PHImageRequestOptions = PHImageRequestOptions()
    options.deliveryMode = .highQualityFormat
    let fetchOptions = PHFetchOptions()
    
    let results: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
    
    if results.count > 0 {
      for i in stride(from: results.count - 1, to: 0, by: -1) {
        let asset = results.object(at: i)
        let size = CGSize(width: 300, height: 300)
        manager.requestImage(for: asset, targetSize: size, contentMode: .default, options: options) { image, _ in
          if let image = image {
            self.images.append(image)
          }
        }
      }
    } else {
      self.errorMessage = "No photos to display"
    }
  }
  
  public func onSelect(with image: UIImage) {
    if self.selectedImages.contains(image) {
      let idx: Int? = self.selectedImages.lastIndex(of: image)
      if let idx = idx, idx != -1 {
        self.selectedImages.remove(at: idx)
      }
    } else if self.selectedImages.count < 5 {
      self.selectedImages.append(image)
    }
  }
}

import SwiftUI
import UIPilot

struct GalleryView: View {
  @StateObject var galleryViewModel = GalleryViewModel()
  @EnvironmentObject var pilot: UIPilot<AppRoute>
  
  let columns = [
    GridItem(.flexible()),
    GridItem(.flexible()),
    GridItem(.flexible())
  ]
  
  var body: some View {
    VStack(spacing: 0) {
      HStack {
        Button {
          pilot.pop()
        } label: {
          HStack(spacing: 4) {
            Image("left-arrow-black")
              .resizable()
              .frame(width: 24, height: 24)
            Text("Back")
              .font(.itim(size: 20))
              .foregroundColor(.blackTitle)
          }
        }
        Spacer()
        
        Text("\(galleryViewModel.selectedImages.count) selected")
          .font(.itim(size: 20))
          .foregroundColor(.blackTitle)
        
        Spacer()
        
        if galleryViewModel.selectedImages.count >= 2 {
          Button {
            pilot.push(.edit(images: $galleryViewModel.selectedImages.wrappedValue))
          } label: {
            Text("Import")
              .font(.itim(size: 20))
              .foregroundColor(.blackTitle)
          }
        } else {
          Text("Import")
            .font(.itim(size: 20))
            .foregroundColor(.clear)
        }
      }
      .padding(.horizontal, 8)
      .frame(width: UIScreen.width, height: 56, alignment: .leading)
      
      if !galleryViewModel.errorMessage.isEmpty {
        VStack {
          Text(galleryViewModel.errorMessage)
            .font(.itim(size: 24))
            .foregroundColor(.blackTitle)
        }
        .padding(.horizontal, 8)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
      } else {
        ScrollView {
          LazyVGrid(columns: columns, spacing: 2) {
            ForEach(galleryViewModel.images, id: \.self) { item in
              ZStack(alignment: .bottomTrailing) {
                Image(uiImage: item)
                  .resizable()
                  .scaledToFill()
                  .frame(width: UIScreen.width / 3, height: UIScreen.width / 3)
                  .clipped()
                  .onTapGesture {
                    galleryViewModel.onSelect(with: item)
                  }
                
                Image(galleryViewModel.selectedImages.contains(item) ? "selected" : "unselected")
                  .resizable()
                  .frame(width: 16, height: 16)
                  .padding(.all, 4)
              }
            }
          }
        }
      }
    }
  }
}

struct GalleryView_Previews: PreviewProvider {
  static var previews: some View {
    GalleryView()
  }
}

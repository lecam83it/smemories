import SwiftUI
import UIPilot

struct CollapseView: View {
  @EnvironmentObject var pilot: UIPilot<AppRoute>
  @StateObject var collapseViewModel = CollapseViewModel()
  @State var imageSize: CGSize?
  @State var imageOrigin: CGPoint?
  
  var images: [UIImage] = []
  init(images: [UIImage]) {
    self.images = images
  }
  
  var body: some View {
    VStack(spacing: 0) {
      HStack {
        Button {
          pilot.popTo(.home)
        } label: {
          HStack(spacing: 4) {
            Image("left-arrow-black")
              .resizable()
              .frame(width: 24, height: 24)
            Text("Discard")
              .font(.itim(size: 20))
              .foregroundColor(.blackTitle)
          }
        }
        
        Spacer()
        
        Button {
          let img = takeScreenshot(origin: CGPoint(x: 0, y: 80),
                                   size: CGSize(width: UIScreen.width, height: UIScreen.width * 4 / 3))
          UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
          pilot.popTo(.home)
        } label: {
          HStack(spacing: 4) {
            Text("Export")
              .font(.itim(size: 20))
              .foregroundColor(.blackTitle)
          }
        }
      }
      .padding(.horizontal, 8)
      .frame(width: UIScreen.width, height: 56, alignment: .leading)
      .zIndex(1)
      
      Color.clear
        .frame(width: UIScreen.width, height: 24)
      
      GeometryReader { geometry in
        EditImageFrame(images: self.images, selected: $collapseViewModel.selected)
      }
      .padding(.all, 4)
      .frame(width: UIScreen.width, height: UIScreen.width * 4 / 3)
      .clipped()
      
      Spacer()
      
      CollapseWidget(
        numberOfPics: self.images.count,
        selected: $collapseViewModel.selected
      )
      .frame(width: UIScreen.width, height: 80)
      .zIndex(1)
    }
  }
  
  func takeScreenshot(origin: CGPoint, size: CGSize) -> UIImage {
    let window = UIWindow(frame: CGRect(origin: origin, size: size))
    let hosting = UIHostingController(rootView: self)
    hosting.view.frame = window.frame
    window.addSubview(hosting.view)
    window.makeKeyAndVisible()
    return hosting.view.renderedImage
  }
}

struct EditImageFrame: View {
  let images: [UIImage]
  @Binding var selected: Int
  
  var body: some View {
    frameView(numberOfPics: images.count, selected: selected)
  }
  
  @ViewBuilder
  func frameView(numberOfPics: Int, selected: Int) -> some View {
    if numberOfPics == 2 {
      frameTwoImages(numberOfPics: numberOfPics, selected: selected)
    } else if numberOfPics == 3 {
      frameThreeImages(numberOfPics: numberOfPics, selected: selected)
    } else if numberOfPics == 4 {
      frameFourImages(numberOfPics: numberOfPics, selected: selected)
    } else if numberOfPics == 5 {
      frameFiveImages(numberOfPics: numberOfPics, selected: selected)
    }
  }
  
  @ViewBuilder
  func frameFiveImages(numberOfPics: Int, selected: Int) -> some View {
    GeometryReader { geometry in
      if numberOfPics == 5 && selected == 0 {
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Image(uiImage: self.images[0])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 4  - 1
              )
              .clipped()
            
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 4 - 2
              )
              .clipped()
            
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 4 - 2
              )
              .clipped()
            
            Image(uiImage: self.images[3])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 4 - 1
              )
              .clipped()
          }
          
          Image(uiImage: self.images[4])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 3 - 1,
              height: geometry.size.height
            )
            .clipped()
        }
      }
      
      if numberOfPics == 5 && selected == 1 {
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Image(uiImage: self.images[0])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3  - 1
              )
              .clipped()
            
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3 - 2
              )
              .clipped()
            
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
          }
          
          VStack(spacing: 2) {
            Image(uiImage: self.images[3])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[4])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
          }
        }
      }
      
      if numberOfPics == 5 && selected == 2 {
        VStack(spacing: 2) {
          HStack(spacing: 2) {
            Image(uiImage: self.images[0])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height * 2 / 3  - 1
              )
              .clipped()
            
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
              .clipped()
          }
          
          HStack(spacing: 2) {
            Image(uiImage: self.images[3])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[4])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
          }
        }
      }
      
      if numberOfPics == 5 && selected == 3 {
        VStack(spacing: 2) {
          HStack(spacing: 2) {
            Image(uiImage: self.images[0])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
          }
          
          Image(uiImage: self.images[2])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 2
            )
            .clipped()
          
          
          HStack(spacing: 2) {
            Image(uiImage: self.images[3])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[4])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
          }
        }
      }
    }
  }
  
  @ViewBuilder
  func frameFourImages(numberOfPics: Int, selected: Int) -> some View {
    GeometryReader { geometry in
      if numberOfPics == 4 && selected == 0 {
        VStack(spacing: 2) {
          HStack(spacing: 2) {
            Image(uiImage: self.images[0])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
          }
          
          HStack(spacing: 2) {
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[3])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
          }
        }
      }
      
      if numberOfPics == 4 && selected == 1 {
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Image(uiImage: self.images[0])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
          }
          
          VStack(spacing: 2) {
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[3])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
              .clipped()
            
          }
        }
      }
      
      if numberOfPics == 4 && selected == 2 {
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Image(uiImage: self.images[0])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
          }
          
          VStack(spacing: 2) {
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[3])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
            
          }
        }
      }
      
      if numberOfPics == 4 && selected == 3 {
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Image(uiImage: self.images[0])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3  - 1
              )
              .clipped()
            
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3 - 2
              )
              .clipped()
            
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3 - 1
              )
              .clipped()
          }
          
          Image(uiImage: self.images[3])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 3 - 1,
              height: geometry.size.height
            )
            .clipped()
        }
      }
    }
  }
  
  @ViewBuilder
  func frameThreeImages(numberOfPics: Int, selected: Int) -> some View {
    GeometryReader { geometry in
      if numberOfPics == 3 && selected == 0 {
        HStack(spacing: 2) {
          Image(uiImage: self.images[0])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 2 - 1,
              height: geometry.size.height
            )
            .clipped()
          
          VStack(spacing: 2) {
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
          }
        }
      }
      
      if numberOfPics == 3 && selected == 1 {
        VStack(spacing: 2) {
          Image(uiImage: self.images[0])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2 - 1
            )
            .clipped()
          
          HStack(spacing: 2) {
            Image(uiImage: self.images[1])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
            
            Image(uiImage: self.images[2])
              .resizable()
              .scaledToFill()
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
              .clipped()
          }
        }
      }
      
      if numberOfPics == 3 && selected == 2 {
        VStack(spacing: 2) {
          Image(uiImage: self.images[0])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 1
            )
            .clipped()
          
          Image(uiImage: self.images[1])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 2
            )
            .clipped()
          
          Image(uiImage: self.images[2])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 1
            )
            .clipped()
        }
      }
      
      if numberOfPics == 3 && selected == 3 {
        HStack(spacing: 2) {
          Image(uiImage: self.images[0])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 3 - 1,
              height: geometry.size.height
            )
            .clipped()
          
          Image(uiImage: self.images[1])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 3 - 2,
              height: geometry.size.height
            )
            .clipped()
          
          Image(uiImage: self.images[2])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 3 - 1,
              height: geometry.size.height
            )
            .clipped()
        }
      }
    }
  }
  
  
  @ViewBuilder
  func frameTwoImages(numberOfPics: Int, selected: Int) -> some View {
    GeometryReader { geometry in
      if numberOfPics == 2 && selected == 0 {
        HStack(spacing: 2) {
          Image(uiImage: self.images[0])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 2 - 1,
              height: geometry.size.height
            )
            .clipped()
          
          Image(uiImage: self.images[1])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 2 - 1,
              height: geometry.size.height
            )
            .clipped()
        }
      }
      
      if numberOfPics == 2 && selected == 1 {
        VStack(spacing: 2) {
          Image(uiImage: self.images[0])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2 - 1
            )
            .clipped()
          
          Image(uiImage: self.images[1])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2 - 1
            )
            .clipped()
        }
      }
      
      if numberOfPics == 2 && selected == 3 {
        VStack(spacing: 2) {
          
          Image(uiImage: self.images[0])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 1
            )
            .clipped()
          
          Image(uiImage: self.images[1])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height * 2 / 3 - 1
            )
            .clipped()
        }
      }
      
      if numberOfPics == 2 && selected == 2 {
        ZStack {
          Image(uiImage: self.images[0])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width,
              height: geometry.size.height
            )
            .clipped()
          
          Image(uiImage: self.images[1])
            .resizable()
            .scaledToFill()
            .frame(
              width: geometry.size.width / 2 + 3,
              height: geometry.size.width / 2 + 3
            )
            .border(.white, width: 2)
            .clipped()
            .offset(
              x: geometry.size.width / 4 + 0.5,
              y: geometry.size.height / 2 - geometry.size.width / 4 + 0.5
            )
        }
      }
    }
  }
}

struct CollapseWidget: View {
  let numberOfPics: Int
  @Binding var selected: Int
  
  var body: some View {
    Group {
      if numberOfPics == 3 {
        HStack {
          Spacer()
          
          ZStack {
            CollapseThreeView01().padding(.all, 6)
            if selected == 0 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 0
          }
          
          Spacer()
          
          ZStack {
            CollapseThreeView02().padding(.all, 6)
            if selected == 1 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 1
          }
          
          Spacer()
          
          ZStack {
            CollapseThreeView03().padding(.all, 6)
            if selected == 2 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 2
          }
          
          Spacer()
          
          ZStack {
            CollapseThreeView04().padding(.all, 6)
            if selected == 3 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 3
          }
          
          Spacer()
        }
        .padding(.horizontal, 16)
      } else if numberOfPics == 2 {
        HStack {
          Spacer()
          ZStack {
            CollapseTwoView01().padding(.all, 6)
            if selected == 0 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 0
          }
          Spacer()
          ZStack {
            CollapseTwoView02().padding(.all, 6)
            if selected == 1 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 1
          }
          Spacer()
          ZStack {
            CollapseTwoView03().padding(.all, 6)
            if selected == 2 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 2
          }
          Spacer()
          ZStack {
            CollapseTwoView04().padding(.all, 6)
            if selected == 3 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 3
          }
          Spacer()
        }
        .padding(.horizontal, 16)
      } else if numberOfPics == 4 {
        HStack {
          Spacer()
          ZStack {
            CollapseFourView01().padding(.all, 6)
            if selected == 0 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 0
          }
          Spacer()
          ZStack {
            CollapseFourView02().padding(.all, 6)
            if selected == 1 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 1
          }
          Spacer()
          ZStack {
            CollapseFourView03().padding(.all, 6)
            if selected == 2 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 2
          }
          Spacer()
          ZStack {
            CollapseFourView04().padding(.all, 6)
            if selected == 3 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 3
          }
          Spacer()
        }
        .padding(.horizontal, 16)
      } else if numberOfPics == 5 {
        HStack {
          Spacer()
          ZStack {
            CollapseFiveView01().padding(.all, 6)
            if selected == 0 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 0
          }
          Spacer()
          ZStack {
            CollapseFiveView02().padding(.all, 6)
            if selected == 1 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 1
          }
          Spacer()
          ZStack {
            CollapseFiveView03().padding(.all, 6)
            if selected == 2 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 2
          }
          Spacer()
          ZStack {
            CollapseFiveView04().padding(.all, 6)
            if selected == 3 {
              Rectangle().strokeBorder(.red, lineWidth: 2)
            }
          }
          .frame(width: 60, height: 80)
          .onTapGesture {
            selected = 3
          }
          Spacer()
        }
        .padding(.horizontal, 16)
      }
    }
  }
}

struct CollapseFiveView01: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 4  - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 4 - 2
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 4 - 2
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 4 - 1
              )
          }
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 3 - 1,
              height: geometry.size.height
            )
        }
      }
    }
  }
}

struct CollapseFiveView02: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3  - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3 - 2
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
          
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height / 2 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height / 2 - 1
              )
          }
        }
      }
    }
  }
}

struct CollapseFiveView03: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height * 2 / 3  - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 3 - 2,
                height: geometry.size.height * 2 / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 3 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
          }
          
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
        }
      }
    }
  }
}


struct CollapseFiveView04: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 2
            )
          
          
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
        }
      }
    }
  }
}



struct CollapseFourView01: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
          }
          
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
          }
        }
      }
    }
  }
}

struct CollapseFourView02: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
          
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3  - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
          }
        }
      }
    }
  }
}

struct CollapseFourView03: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
          
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height * 2 / 3  - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
        }
      }
    }
  }
}

struct CollapseFourView04: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3  - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3 - 2
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width * 2 / 3 - 1,
                height: geometry.size.height / 3 - 1
              )
          }
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 3 - 1,
              height: geometry.size.height
            )
        }
      }
    }
  }
}


struct CollapseTwoView01: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 2 - 1,
              height: geometry.size.height
            )
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 2 - 1,
              height: geometry.size.height
            )
        }
      }
    }
  }
}

struct CollapseTwoView02: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2 - 1
            )
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2 - 1
            )
        }
      }
    }
  }
}

struct CollapseTwoView04: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 1
            )
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height * 2 / 3 - 1
            )
        }
      }
    }
  }
}

struct CollapseTwoView03: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        ZStack {
          Rectangle().fill(Color.lightGray)
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 2 + 3,
              height: geometry.size.width / 2 + 3
            )
            .border(.white, width: 2)
            .offset(
              x: geometry.size.width / 4 + 0.5,
              y: geometry.size.height / 2 - geometry.size.width / 4 + 0.5
            )
        }
      }
    }
  }
}

struct CollapseThreeView01: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 2 - 1,
              height: geometry.size.height
            )
          
          VStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
          }
        }
      }
    }
  }
}

struct CollapseThreeView02: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2
            )
          
          HStack(spacing: 2) {
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
            
            Rectangle().fill(Color.lightGray)
              .frame(
                width: geometry.size.width / 2 - 1,
                height: geometry.size.height / 2 - 1
              )
          }
        }
      }
    }
  }
}

struct CollapseThreeView03: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        VStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 1
            )
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 2
            )
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 3 - 1
            )
        }
      }
    }
  }
}

struct CollapseThreeView04: View {
  var body: some View {
    ZStack(alignment: .center) {
      GeometryReader { geometry in
        HStack(spacing: 2) {
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 3 - 1,
              height: geometry.size.height
            )
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 3 - 2,
              height: geometry.size.height
            )
          
          Rectangle().fill(Color.lightGray)
            .frame(
              width: geometry.size.width / 3 - 1,
              height: geometry.size.height
            )
        }
      }
    }
  }
}

struct CollapseThreeView05: View {
  var body: some View {
    ZStack {
      GeometryReader { geometry in
        Rectangle().fill(.red)
        Parallelogram().fill(Color.lightGray)
        ZStack {
          Triangle().fill(Color.lightGray)
            .rotationEffect(.radians(.pi))
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2
            )
          
          Triangle().fill(Color.lightGray)
            .offset(y: geometry.size.height / 2)
            .frame(
              width: geometry.size.width,
              height: geometry.size.height / 2
            )
          
        }
      }
    }
  }
}

struct CollapseView_Previews: PreviewProvider {
  static var previews: some View {
    CollapseView(images: [])
  }
}

import SwiftUI

struct Parallelogram: Shape {
  func path(in rect: CGRect) -> Path {
    var path = Path()
    
    path.move(to: CGPoint(x: rect.minX, y: rect.minY + 1))
    path.addLine(to: CGPoint(x: rect.minX, y: rect.midY - 1))
    path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - 1))
    path.addLine(to: CGPoint(x: rect.maxX, y: rect.midY + 1))
    path.addLine(to: CGPoint(x: rect.minX, y: rect.minY + 1))
    
    return path
  }
}

struct Parallelogram_Previews: PreviewProvider {
  static var previews: some View {
    Parallelogram()
      .fill(.blue)
      .frame(width: 200, height: 200, alignment: .center)
      .background(Color.red)
  }
}


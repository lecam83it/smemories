import SwiftUI

struct CarouselView<Content:View>: View {
  private var numberOfItems: Int
  private var content: Content
  @State var slideGesture: CGSize = CGSize.zero
  @State private var currentIndex: Int = 0
  private let timer = Timer.publish(every: 3, on: .main, in: .common).autoconnect()
  private let threshout = 50.0
  init(numberOfItems: Int, @ViewBuilder content: () -> Content) {
    self.numberOfItems = numberOfItems
    self.content = content()
  }


  var body: some View {
    GeometryReader { geometry in
      VStack(alignment: .trailing, spacing: 0) {
        HStack(spacing: 0) {
          self.content
            .padding(.all, 8)
            .frame(width: UIScreen.width, height: UIScreen.width * 4 / 3)
        }
        .frame(width: geometry.size.width, height: geometry.size.height, alignment: .leading)
        .offset(x: CGFloat(self.currentIndex) * -geometry.size.width, y: 0)
        .animation(.spring(), value: CGFloat(self.currentIndex) * -geometry.size.width)
        .onReceive(self.timer) { _ in
          self.currentIndex = (self.currentIndex + 1) % self.numberOfItems
        }
        .gesture(
          DragGesture().onChanged({ value in
            self.slideGesture = value.translation
          })
          .onEnded({ _ in
            if self.slideGesture.width < -threshout {
              if self.currentIndex < self.numberOfItems - 1 {
                withAnimation {
                  self.currentIndex += 1
                }
              }
            }
            if self.slideGesture.width > threshout {
              if self.currentIndex > 0 {
                withAnimation {
                  self.currentIndex -= 1
                }
              }
            }
            self.slideGesture = .zero
          })
        )

        HStack(spacing: 4) {
          ForEach(0 ..< self.numberOfItems) { idx in
            RoundedRectangle(cornerRadius: 8)
              .fill(self.currentIndex == idx ? Color.activeDot : Color.inactiveDot)
              .frame(width: self.currentIndex == idx ? 16 : 8, height: 8)
          }
        }
        .padding(.horizontal, 8)
      }
    }
  }
}


struct CarouselView_Previews: PreviewProvider {
  static var previews: some View {
    CarouselView(numberOfItems: 5) {

    }
  }
}

import SwiftUI

struct Triangle: Shape {
  func path(in rect: CGRect) -> Path {
    var path = Path()
    
    path.move(to: CGPoint(x: rect.minX, y: rect.minY + 1))
    path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY + 1))
    path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY + 1))
    path.addLine(to: CGPoint(x: rect.minX, y: rect.minY + 1))
    
    return path
  }
}

struct Triangle_Previews: PreviewProvider {
  static var previews: some View {
    Triangle()
  }
}

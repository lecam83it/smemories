import SwiftUI
import AVFoundation
import UIPilot

struct CameraView: View {
  
  @StateObject var cameraViewModel = CameraViewModel()
  @EnvironmentObject var pilot: UIPilot<AppRoute>
  
  var body: some View {
    NavigationView {
      VStack {
        Button {
          pilot.pop()
        } label: {
          HStack(spacing: 4) {
            Image("left-arrow-white")
              .resizable()
              .frame(width: 24, height: 24)
            Text("Back")
              .font(.itim(size: 20))
              .foregroundColor(.white)
          }
        }
        .padding(.horizontal, 8)
        .frame(width: UIScreen.width, height: 56, alignment: .leading)
        
        Spacer()
        
        CameraPreview(cameraSession: cameraViewModel.cameraSession)
          .frame(height: UIScreen.width * 4 / 3)
        
        
        Spacer()
        
        HStack {
          if cameraViewModel.lastestImage != nil {
            Image(uiImage: cameraViewModel.lastestImage!)
              .resizable()
              .frame(width: 64, height: 64, alignment: .center)
              .cornerRadius(8)
              .overlay(RoundedRectangle(cornerRadius: 8).stroke(.white, lineWidth: 1))
          } else {
            Color.black
              .frame(width: 64, height: 64, alignment: .center)
          }
          
          Spacer()
          
          TakePhotoButton(
            onPressed: {
              cameraViewModel.takePhoto()
            },
            numberOfPics: $cameraViewModel.numberOfPics
          )
          
          Spacer()
          
          if cameraViewModel.images.count >= 2 {
            Button {
              pilot.push(.edit(images: $cameraViewModel.images.wrappedValue))
            } label: {
              Text("Finish")
                .font(.itim(size: 20))
                .foregroundColor(.white)
                .frame(width: 64, height: 64, alignment: .center)
            }
          } else {
            Color.black
              .frame(width: 64, height: 64, alignment: .center)
          }
        }
        .frame(maxWidth: .infinity)
        .padding(.horizontal, 20)
      }
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .background(Color.black)
      .navigationBarHidden(true)
      .navigationBarBackButtonHidden(true)
    }
  }
}

struct TakePhotoButton: View {
  
  let onPressed: () -> Void
  @Binding var numberOfPics: Int
  
  var body: some View {
    if numberOfPics == 5 {
      ZStack {
        Circle()
          .fill(.white.opacity(0.6))
          .frame(width: 64, height: 64)
        
        Circle()
          .strokeBorder(.white.opacity(0.6), lineWidth: 2)
          .frame(width: 72, height: 72)
        
        Text("\(numberOfPics)")
          .font(.itim(size: 28))
          .foregroundColor(.black.opacity(0.6))
      }
    } else {
      Button {
        onPressed()
      } label: {
        ZStack {
          Circle()
            .fill(.white)
            .frame(width: 64, height: 64)
          
          Circle()
            .strokeBorder(.white, lineWidth: 2)
            .frame(width: 72, height: 72)
          
          Text("\(numberOfPics)")
            .font(.itim(size: 28))
            .foregroundColor(.black)
        }
      }
    }
    
  }
}

struct CameraPreview: UIViewControllerRepresentable {
  class CameraPreviewView: UIView {
    override class var layerClass: AnyClass {
      AVCaptureVideoPreviewLayer.self
    }
    
    var cameraPreviewView: AVCaptureVideoPreviewLayer {
      return layer as! AVCaptureVideoPreviewLayer
    }
  }
  
  final class CameraViewController : UIViewController {
    override func loadView() {
      view = CameraPreviewView()
    }
    
    private var cameraView: CameraPreviewView { view as! CameraPreviewView }
    
    var cameraSessionVC: AVCaptureSession?
    
    override func viewDidAppear(_ animated: Bool) {
      cameraView.cameraPreviewView.cornerRadius = 0
      cameraView.cameraPreviewView.connection?.videoOrientation = .portrait
    }

    override func viewWillAppear(_ animated: Bool) {
      cameraView.cameraPreviewView.session = cameraSessionVC
      cameraView.cameraPreviewView.session?.sessionPreset = .photo
    }
  }
  
  let cameraSession: AVCaptureSession
  
  func makeUIViewController(context: Context) -> CameraViewController {
    let vc = CameraViewController()
    vc.cameraSessionVC = cameraSession
    return vc
  }
  
  func updateUIViewController(_ uiViewController: CameraViewController, context: Context) {
    
  }
}

struct CameraView_Previews: PreviewProvider {
  static var previews: some View {
    CameraView()
  }
}

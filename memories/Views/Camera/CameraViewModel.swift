import Foundation
import AVFoundation
import UIKit

class CameraViewModel: ObservableObject {

  private let cameraManager: CameraManager = CameraManager.shared

  var cameraSession: AVCaptureSession

  init() {
    self.cameraSession = cameraManager.cameraSession
  }


  public var images: [UIImage] = []

  @Published var numberOfPics: Int = 0
  @Published var lastestImage: UIImage? = nil

  func takePhoto() {
    cameraManager.takePhoto { image in
      self.numberOfPics += 1
      self.lastestImage = image
      self.images.append(image)
    }
  }
}

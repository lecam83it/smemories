import Foundation
import AVFoundation
import UIKit

class CameraManager: NSObject {

  static let shared = CameraManager()

  private override init() {}

  let cameraSession = AVCaptureSession()

  let sessionQueue = DispatchQueue(label: "com.dragold.camera-memories")

  var data = Data(count: 0)

  private let photoOutput = AVCapturePhotoOutput()

  enum CameraStatus {
    case configured
    case unconfigured
    case unauthorized
    case failed
  }

  private var status: CameraStatus = .unconfigured
  var isRunning = false
  var isConfigured = false

  public func checkPermissions() {
    switch AVCaptureDevice.authorizationStatus(for: .video) {
      case .notDetermined:
        sessionQueue.suspend()
        AVCaptureDevice.requestAccess(for: .video) { authorized in
          if !authorized {
            self.status = .unauthorized
          } else {
            self.status = .configured
          }
          self.sessionQueue.resume()
        }
        return

      case .authorized:
        self.status = .configured
        return

      case .restricted:
        status = .unauthorized
        return
      case .denied:
        status = .unauthorized
        return
      @unknown default:
        status = .unauthorized
        return

    }
  }

  public func configuration() {
    print("configuration")
    sessionQueue.async {
      self.configureCaptureSession()
    }
  }

  private func configureCaptureSession() {
    guard status == .configured else {
      return
    }

    cameraSession.beginConfiguration()
    
    do {
      let defaultVideoDevice: AVCaptureDevice? = defaultCamera()

      guard defaultVideoDevice != nil else {
        status = .failed
        cameraSession.commitConfiguration()
        return
      }

      let input = try AVCaptureDeviceInput(device: defaultVideoDevice!)
      
      if self.cameraSession.canAddInput(input) {
        self.cameraSession.addInput(input)
        print("Create video device input!")
      } else {
        print("Couldn't create video device input!")
        self.status = .failed
        self.cameraSession.commitConfiguration()
        return
      }
    } catch {
      print(error.localizedDescription)
      status = .failed
      self.cameraSession.commitConfiguration()
      return
    }

    if self.cameraSession.canAddOutput(self.photoOutput) {
      self.cameraSession.addOutput(self.photoOutput)
      self.photoOutput.isHighResolutionCaptureEnabled = true
      self.photoOutput.maxPhotoQualityPrioritization = .quality
    } else {
      self.status = .failed
      self.cameraSession.commitConfiguration()
      return
    }

    cameraSession.commitConfiguration()

    self.isConfigured = true

    self.startCamera()
  }

  private func defaultCamera() -> AVCaptureDevice? {
    if let device = AVCaptureDevice.default(.builtInDualCamera,
                                            for: AVMediaType.video,
                                            position: .back) {
      return device
    } else if let device = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                   for: AVMediaType.video,
                                                   position: .back) {
      return device
    } else {
      return nil
    }
  }

  public func startCamera() {
    DispatchQueue.global().async {
      print("isNotRunning: \(!self.isRunning)")
      print("isConfigured: \(self.isConfigured)")
      print("status: \(self.status)")
      if !self.isRunning && self.isConfigured {
        switch self.status {
          case .configured:
            self.cameraSession.startRunning()
            self.isRunning = self.cameraSession.isRunning
            print("Camera Session is Running")
            return

          case .unauthorized, .unconfigured, .failed:
            print("Application not authorized to use camera")
        }
      }
    }
  }

  private var completionHandler: ((UIImage) -> Void)?

  public func takePhoto(completionHandler: @escaping (UIImage) -> Void) {
    self.completionHandler = completionHandler
    sessionQueue.async {
      self.photoOutput.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
    }
  }

  public func savePhoto() {
    let image = UIImage(data: self.data)!
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    self.completionHandler!(image)
  }
}

extension CameraManager: AVCapturePhotoCaptureDelegate {
  func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
    if error != nil {
      return
    }

    guard let imageData = photo.fileDataRepresentation() else {
      return
    }

    self.data = imageData

    self.savePhoto()
  }
}

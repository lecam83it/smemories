import SwiftUI

extension Color {
  static let inactiveDot = Color.init(hex: "EDEDED")
  static let activeDot = Color.init(hex: "0D0B0B")
  static let blackTitle = Color.init(hex: "0D0B0B")
  static let lightGray = Color.init(hex: "EDEDED")

  init(hex: String, with alpha: Double = 1.0) {
    let scanner = Scanner(string: hex)
    scanner.currentIndex = hex.startIndex
    var rgbValue: UInt64 = 0
    scanner.scanHexInt64(&rgbValue)

    let r = (rgbValue & 0xff0000) >> 16
    let g = (rgbValue & 0xff00) >> 8
    let b = rgbValue & 0xff

    self.init(
      red: CGFloat(r) / 0xff,
      green: CGFloat(g) / 0xff,
      blue: CGFloat(b) / 0xff,
      opacity: alpha
    )
  }
}


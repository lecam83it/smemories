import SwiftUI

extension Font {
  static func itim(size: CGFloat) -> Font {
    return Font.custom("Itim-Regular", size: size)
  }
}
